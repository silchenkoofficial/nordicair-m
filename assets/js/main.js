const dropdownBtn = document.querySelector('.nav__buttons--menu');
const dropdownMenu = document.querySelector('.nav__dropdown-menu');

dropdownBtn.addEventListener('click', () => {
   if (dropdownMenu.style.display != 'flex') {
      $('.nav__dropdown-menu').css('display', 'flex');
      $('body').css('overflow', 'hidden');
   }
});

$(".dropdown__close").on('click', function () {
   $(".dropdown").css('display', 'none');
   $('body').css('overflow', 'auto');
});

// ==========================================================================
let brendName = [
   'Aero', 'Aerolite', 'Aeronik', 'Airwell', 'Axioma',
   'Ballu',
   'Carrier', 'Chigo',
   'Daikin', 'Dahatsu',
   'Electrolux',
   'Fujitsu',
   'General', 'Gree',
   'Haier', 'Hitachi', 'Hisense',
   'IGC',
   'Kentatsu',
   'Lanzkraft', 'Leberg', 'Lessar', 'LG',
   'MDV', 'Midea', 'Mitsubishi Electric',
   'Panasonic',
   'Quattroclima',
   'Roda', 'Royal Clima',
   'Samsung',
   'TCL', 'Toshiba', 'Tosot',
   'Zanussi'
]

for (let i = 0; i < brendName.length; i++) {
   $('.brend__list').append('<li class="">' + brendName[i] + '</li>');
}

$('.brend__btn').on('click', function () {
   if ($('.brend__list').css('display', 'none')) {
      $('.brend-arrow').css('transform', 'rotate(0deg)');
      $('.brend__list').css('display', 'block');
   }
});

$('.brend__list li').on('click', function () {
   $('.brend__list li').removeClass('active');
   $(this).toggleClass('active');
});

$('.head__exp').on('click', function () {
   $(this).toggleClass('active');
   $('.head__cheap').toggleClass('active');
});

$('.head__cheap').on('click', function () {
   $(this).toggleClass('active');
   $('.head__exp').toggleClass('active');
});

$('.head__filter').on('click', function () {
   $(this).toggleClass('active-filter');
   if ($('.head__filter').hasClass('active-filter')) {
      $('.filter').css('display', 'block');
      $('.catalog__main').css('display', 'none');
      $('.catalog__moreBtn').css('display', 'none');
      $('.footer').css('display', 'none');
   } else {
      $('.filter').css('display', 'none');
      $('.catalog__main').css('display', 'flex');
      $('.catalog__moreBtn').css('display', 'flex');
      $('.footer').css('display', 'block');
   }
});

$('.filter__submit').on('click', function () {
   $('.filter').css('display', 'none');
   $('.catalog__main').css('display', 'flex');
   $('.catalog__moreBtn').css('display', 'flex');
   $('.footer').css('display', 'block');
   $('.head__filter').toggleClass('active-filter');
});

$('.card__more').on('click', () => {
   location.href = 'product-page-m';
});
// ================================================================
$('.btn__desc').on('click', function () {
   $(this).toggleClass('btn__active');
   $('.btn__characteristics').toggleClass('btn__active');
   $('.change__description').css('display', 'block');
   $('.change__characteristics').css('display', 'none');
});

$('.btn__characteristics').on('click', function () {
   $(this).toggleClass('btn__active');
   $('.btn__desc').toggleClass('btn__active');
   $('.change__description').css('display', 'none');
   $('.change__characteristics').css('display', 'block');
});
// ================================================================