<!-- === FOOTER ========================================================== -->
        <footer class="footer">
            <div class="container">
               <div class="wrapper">
                  <div class="footer__social social">
                     <div class="social__viber"></div>
                     <div class="social__whatsapp"></div>
                     <div class="social__instagram"></div>
                  </div>
                  <nav class="footer__nav nav">
                     <ul class="nav__list list">
                        <li><a href="javascript://" class="list__link">Контакты<i class="fa fa-caret-down" aria-hidden="true"></i></a></li>
                        <li><a href="javascript://" class="list__link">Услуги<i class="fa fa-caret-down" aria-hidden="true"></i></a></li>
                        <li><a href="javascript://" class="list__link">Наши работы<i class="fa fa-caret-down" aria-hidden="true"></i></a></li>
                        <li><a href="javascript://" class="list__link">О нас<i class="fa fa-caret-down" aria-hidden="true"></i></a></li>
                        <li><a href="javascript://" class="list__link">Партнерам<i class="fa fa-caret-down" aria-hidden="true"></i></a></li>
                     </ul>
                  </nav>
                  <div class="footer__copyright">
                     <p>© NordicAir, 2020</p>
                  </div>
               </div>
            </div>
         </footer>
<!-- ===================================================================== -->
         <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
         <script src="<?php echo get_template_directory_uri(); ?>/assets/js/main.js"></script>
    </body>
</html>
