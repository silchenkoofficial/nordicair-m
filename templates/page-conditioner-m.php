<?php 
/*
Template Name: Кондиционирование m
*/
?>

<?php get_header(); ?>

<!-- НАВИГАЦИЯ =================== -->
   <section class="navigation">
      <div class="container">
         <div class="wrapper">
            <div class="navigation__title">Кондиционирование</div>
            <div class="navigation__body body">
               <div class="body__card card">
                  <div class="card__image">
                     <img src="<?php echo get_template_directory_uri(); ?>/assets/images/content/navigationPages/conditioner-1.png" alt="">
                  </div>
                  <div class="card__main">
                     <div class="card__text text">
                        <div class="text__head">
                           Настенные сплит-системы
                        </div>
                        <div class="text__desc">
                           Настенные кондиционеры для дома, квартиры, офиса или магазина. В данном каталоге представлены
                           только лучшие решения!
                        </div>
                     </div>
                     <div class="card__button" onclick="location.href = 'catalog-wall-m'">В каталог</div>
                  </div>
               </div>
               <div class="body__card card">
                  <div class="card__image">
                     <img src="<?php echo get_template_directory_uri(); ?>/assets/images/content/navigationPages/conditioner-2.png" alt="">
                  </div>
                  <div class="card__main">
                     <div class="card__text text">
                        <div class="text__head">
                           Мульти сплит-системы
                        </div>
                        <div class="text__desc">
                           Требуется установить несколько систем в одном помещении? Тогда такое решение незаменимо для вас!
                           Удобно. Качественно. Надёжно.
                        </div>
                     </div>
                     <div class="card__button" onclick="location.href = 'catalog-multy-m'">В каталог</div>
                  </div>
               </div>
               <div class="body__card card">
                  <div class="card__image">
                     <img src="<?php echo get_template_directory_uri(); ?>/assets/images/content/navigationPages/conditioner-3.png" alt="">
                  </div>
                  <div class="card__main">
                     <div class="card__text text">
                        <div class="text__head">
                           Полупромышленные сплит-системы
                        </div>
                        <div class="text__desc">
                           От охлаждения серверных помещений до создания комфортной системы кондиционирования офисного
                           здания!
                        </div>
                     </div>
                     <div class="card__button" onclick="location.href = 'catalog-semiIndustrial-m'">В каталог</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
<!-- ============================= -->

<?php get_footer(); ?>