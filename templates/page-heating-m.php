<?php 
/*
Template Name: Отопление m
*/
?>

<?php get_header(); ?>

<!-- НАВИГАЦИЯ =================== -->
   <section class="heating">
      <div class="wrapper">
         <div class="heating__title">Отопление</div>
         <div class="heating__body body">
            <div class="body__card">
               <img src="<?php echo get_template_directory_uri(); ?>/assets/images/content/navigationPages/heating-block.png" alt="" class="bg-image">
               <div class="card__block block">
                  <div class="block__main">
                     <div class="block__title">Водяные</div>
                     <div class="block__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/content/navigationPages/heating-1.png" alt="">
                     </div>
                     <div class="block__text">
                        Самые мощные
                        приборы в линейке высоконапорных завес, основным источником тепловой энергии которых является
                        горячая вода.
                     </div>
                  </div>
                  <div class="block__button left">В каталог</div>
               </div>
               <div class="card__block block">
                  <div class="block__main">
                     <div class="block__title">Электрические</div>
                     <div class="block__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/content/navigationPages/heating-2.png" alt="">
                     </div>
                     <div class="block__text">
                        Создадут невидимый барьер воздушного потока. В нашем каталоге только
                        самые эффективные
                        модели!
                     </div>
                  </div>
                  <div class="block__button right">В каталог</div>
               </div>
            </div>
            <div class="body__card">
               <img src="<?php echo get_template_directory_uri(); ?>/assets/images/content/navigationPages/heating-block.png" alt="" class="bg-image">
               <h1 class="pumps__title">Тепловые насосы</h1>
               <div class="card__block block">
                  <div class="block__main">
                     <div class="block__title">Воздух-воздух</div>
                     <div class="block__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/content/navigationPages/heating-3.png" alt="">
                     </div>
                     <div class="block__text">
                        Обеспечивают значительную экономию расходов на обогрев. Теплонасосы способны как охлаждать, так и
                        нагревать воздушные массы в доме.
                     </div>
                  </div>
                  <div class="block__button left">В каталог</div>
               </div>
               <div class="card__block block">
                  <div class="block__main">
                     <div class="block__title">Воздух-вода</div>
                     <div class="block__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/content/navigationPages/heating-4.png" alt="">
                     </div>
                     <div class="block__text">
                        Идеальное использование для небольших помещений, а также простая установка не требующая скважин или
                        дополнительной площади!
                     </div>
                  </div>
                  <div class="block__button right">В каталог</div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- ============================= -->

<?php get_footer(); ?>