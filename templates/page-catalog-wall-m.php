<?php 
/*
Template Name: Каталог wall m
*/
?>

<?php get_header(); ?>

<section class="catalog__title" style="
   text-align: center;
   margin-top: 40px;
   display: flex;
   align-items: center;
   justify-content: center;
   font-weight: bold;
   font-size: 1.5rem;
">
   Мульти сплит-системы
</section>
<!-- КАТАЛОГ =================== -->
   <section class="catalog">
      <div class="container">
         <div class="wrapper">
            <div class="catalog__head head">
               <div class="head__exp">сначала дорогие</div>
               <div class="head__cheap active">сначала дешевые</div>
               <div class="head__filter"><i class="fa fa-filter" aria-hidden="true"></i></div>
            </div>
         </div>
      </div>
      <div class="filter">
         <div class="container">
            <div class="wrapper">
                  <form>
                     <div class="filter__brend brend">
                        <div class="brend__btn title">Производитель<i class="fa fa-sort-desc brend-arrow" aria-hidden="true"></i></div>
                        <ul class="brend__list list"></ul>
                     </div>
                     <div class="filter__price price">
                        <div class="price__title title">Цена</div>
                        <div class="price__main main">
                           <input type="text" class="price__input" placeholder="0">
                           <span></span>
                           <input type="text" class="price__input" placeholder="419 600">
                        </div>
                     </div>
                     <div class="filter__invertor invertor">
                        <div class="invertor__title title">Инвертор</div>
                        <div class="invertor__main main">
                           <label for="invertor-yes" class="invertor__label label">
                              <input type="radio" id="invertor-yes" name="invertor" checked>
                              <p>Есть</p>
                           </label>
                           <label for="invertor-no" class="invertor__label label">
                              <input type="radio" id="invertor-no" name="invertor">
                              <p>Нет</p>
                           </label>
                        </div>
                     </div>
                     <div class="filter__work-time work-time">
                        <div class="work-time__title title">Режим работы</div>
                        <div class="work-time__main main">
                           <label for="work-time-1" class="work-time__label label">
                              <input type="radio" id="work-time-1" name="work-time" checked>
                              <p>Охлаждение и обогрев</p>
                           </label>
                           <label for="work-time-2" class="work-time__label label">
                              <input type="radio" id="work-time-2" name="work-time">
                              <p>Только охдаждение</p>
                           </label>
                        </div>
                     </div>
                     <div class="filter__stock stock">
                        <div class="stock__title title">Наличие</div>
                        <div class="stock__main main">
                           <label for="stock-1" class="stock__label label">
                              <input type="radio" id="stock-1" name="stock" checked>
                              <p>В наличии</p>
                           </label>
                           <label for="stock-2" class="stock__label label">
                              <input type="radio" id="stock-2" name="stock">
                              <p>На заказ</p>
                           </label>
                        </div>
                     </div>
                     <div class="filter__square square">
                        <div class="square__title title">Площадь помещения</div>
                        <div class="square__main main">
                           <input type="text" class="square__input" placeholder="от">
                           <span></span>
                           <input type="text" class="square__input" placeholder="до">
                        </div>
                     </div>
                     <div class="filter__power power">
                        <div class="power__title title">Мощность</div>
                        <div class="power__main main">
                           <input type="text" class="power__input" placeholder="2,5">
                           <span></span>
                           <input type="text" class="power__input" placeholder="18">
                        </div>
                     </div>
                     <div class="filter__guarantee guarantee">
                        <div class="guarantee__title title">Гарантия</div>
                        <div class="guarantee__main main">
                           <label for="guarantee-1" class="guarantee__label label">
                              <input type="radio" id="guarantee-1" name="guarantee" checked>
                              <p>1 год</p>
                           </label>
                           <label for="guarantee-2" class="guarantee__label label">
                              <input type="radio" id="guarantee-2" name="guarantee">
                              <p>4 года</p>
                           </label>
                           <label for="guarantee-3" class="guarantee__label label">
                              <input type="radio" id="guarantee-3" name="guarantee">
                              <p>1.5 года</p>
                           </label>
                           <label for="guarantee-4" class="guarantee__label label">
                              <input type="radio" id="guarantee-4" name="guarantee">
                              <p>5 лет</p>
                           </label>
                           <label for="guarantee-5" class="guarantee__label label">
                              <input type="radio" id="guarantee-5" name="guarantee">
                              <p>2 года</p>
                           </label>
                           <label for="guarantee-6" class="guarantee__label label">
                              <input type="radio" id="guarantee-6" name="guarantee">
                              <p>7 лет</p>
                           </label>
                           <label for="guarantee-7" class="guarantee__label label">
                              <input type="radio" id="guarantee-7" name="guarantee">
                              <p>3 года</p>
                           </label>
                        </div>
                     </div>
                     <div class="filter__submit">Применить</div>
                     <!-- <div class="filter__reset"><p>Сбросить фильтр</p></div> -->
                  </form>
            </div>
         </div>
      </div>
      <div class="wrapper">
         <div class="catalog__main main">
            <div class="main__card card">
               <div class="card__image"></div>
               <div class="card__title">Кондиционер 1</div>
               <div class="card__price">10 000 ₽</div>
               <div class="card__inStock"><i class="fa fa-check-circle" aria-hidden="true"></i>В наличии</div>
               <div class="card__more">Подробнее</div>
            </div>
            <div class="main__card card">
               <div class="card__image"></div>
               <div class="card__title">Кондиционер 1</div>
               <div class="card__price">10 000 ₽</div>
               <div class="card__inStock"><i class="fa fa-check-circle" aria-hidden="true"></i>В наличии</div>
               <div class="card__more">Подробнее</div>
            </div>
            <div class="main__card card">
               <div class="card__image"></div>
               <div class="card__title">Кондиционер 1</div>
               <div class="card__price">10 000 ₽</div>
               <div class="card__inStock"><i class="fa fa-check-circle" aria-hidden="true"></i>В наличии</div>
               <div class="card__more">Подробнее</div>
            </div>
            <div class="main__card card">
               <div class="card__image"></div>
               <div class="card__title">Кондиционер 1</div>
               <div class="card__price">10 000 ₽</div>
               <div class="card__inStock"><i class="fa fa-check-circle" aria-hidden="true"></i>В наличии</div>
               <div class="card__more">Подробнее</div>
            </div>
            <div class="main__card card">
               <div class="card__image"></div>
               <div class="card__title">Кондиционер 1</div>
               <div class="card__price">10 000 ₽</div>
               <div class="card__inStock"><i class="fa fa-check-circle" aria-hidden="true"></i>В наличии</div>
               <div class="card__more">Подробнее</div>
            </div>
            <div class="main__card card">
               <div class="card__image"></div>
               <div class="card__title">Кондиционер 1</div>
               <div class="card__price">10 000 ₽</div>
               <div class="card__inStock"><i class="fa fa-check-circle" aria-hidden="true"></i>В наличии</div>
               <div class="card__more">Подробнее</div>
            </div>
         </div>
         <div class="catalog__moreBtn">
            <div class="catalog__moreBtn--dots">
               <img src="https://img.icons8.com/windows/25/0D56A6/ellipsis.png" />
            </div>
            Показать больше товаров
         </div>
      </div>
   </section>
<!-- ============================= -->

<?php get_footer(); ?>