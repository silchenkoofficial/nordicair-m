<?php 
/*
Template Name: Вентиляция m
*/
?>

<?php get_header(); ?>

<!-- НАВИГАЦИЯ =================== -->
   <section class="navigation">
      <div class="container">
         <div class="wrapper">
            <div class="navigation__title">Вентиляционные
               установки</div>
            <div class="navigation__body body">
               <div class="body__card card">
                  <div class="card__image">
                     <img src="<?php echo get_template_directory_uri(); ?>/assets/images/content/navigationPages/ventilation-1.png" alt="">
                  </div>
                  <div class="card__main">
                     <div class="card__text text">
                        <div class="text__head">
                           Приточно-вытяжная вентиляция
                        </div>
                        <div class="text__desc">
                           Самый надежный способ очищения воздуха в доме!
                        </div>
                     </div>
                     <div class="card__button" onclick="location.href = 'catalog-supply-m'">В каталог</div>
                  </div>
               </div>
               <div class="body__card card">
                  <div class="card__image">
                     <img src="<?php echo get_template_directory_uri(); ?>/assets/images/content/navigationPages/ventilation-2.png" alt="">
                  </div>
                  <div class="card__main">
                     <div class="card__text text">
                        <div class="text__head">
                           Рекуператоры
                        </div>
                        <div class="text__desc">
                           На улицу выходит полностью «отработанный» воздух, а в помещение попадает не только свежий, но и
                           уже нагретый воздух.
                        </div>
                     </div>
                     <div class="card__button">В каталог</div>
                  </div>
               </div>
               <div class="body__card card">
                  <div class="card__image">
                     <img src="<?php echo get_template_directory_uri(); ?>/assets/images/content/navigationPages/ventilation-3.png" alt="">
                  </div>
                  <div class="card__main">
                     <div class="card__text text">
                        <div class="text__head">
                           Бризеры
                        </div>
                        <div class="text__desc">
                           Компактная приточная вентиляция с подогревом, очисткой воздуха и умным управлением со смартфона.
                        </div>
                     </div>
                     <div class="card__button">В каталог</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- ============================= -->

<?php get_footer(); ?>