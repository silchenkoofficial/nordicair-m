<?php 
/*
Template Name: Страница товара m
*/
?>

<?php get_header(); ?>

<!-- === Страница товара ========================== -->
   <section class="product">
      <div class="wrapper">
         <div class="container">
            <div class="product__title">Кондиционер Aero<br>ALRS-II-09IHA4-01/ALRS-II-09OHA4-01</div>
            <div class="product__image">
               <img src="<?php echo get_template_directory_uri(); ?>/assets/images/content/product/product-photo.png" alt="">
            </div>
            <div class="product__property property">
               <div class="property__title">Основные характеристики:</div>
               <div class="property__block block">
                  <div class="brend">
                     <span class="key">Производитель:</span>
                     <span class="dotted"></span>
                     <span class="value blue">Aero</span>
                  </div>
                  <div class="model">
                     <span class="key">Модель:</span>
                     <span class="dotted"></span>
                     <span class="value blue">AeroLite</span>
                  </div>
                  <div class="country">
                     <span class="key">Страна сборки:</span>
                     <span class="dotted"></span>
                     <span class="value">Китай</span>
                  </div>
                  <div class="square">
                     <span class="key">Площадь помещения:</span>
                     <span class="dotted"></span>
                     <span class="value">25 кв.м.</span>
                  </div>
                  <div class="mode">
                     <span class="key">Режим работы:</span>
                     <span class="dotted"></span>
                     <span class="value blue">Охлаждение и обогрев</span>
                  </div>
                  <div class="guarantee">
                     <span class="key">Гарантийный срок:</span>
                     <span class="dotted"></span>
                     <span class="value">3 года</span>
                  </div>
               </div>
            </div>
            <div class="product__price">
               <p>10 000 ₽</p>
               <div class="product__stoke">
                  <i class="fa fa-check-circle" aria-hidden="true"></i>
                  В наличии
               </div>
            </div>
            <div class="product__cart">
               <i class="fa fa-shopping-basket" aria-hidden="true"></i>
               Добавить в корзину
            </div>
         </div>
         <div class="product__change change">
            <div class="container">
               <div class="change__buttons btn">
                  <div class="btn__desc btn__active">Описание</div>
                  <div class="btn__characteristics">Характеристика товара</div>
               </div>
            </div>
            <div class="change__description description">
               <img src="<?php echo get_template_directory_uri(); ?>/assets/images/content/product/description-photo.png" alt="">
               <div class="description__body">
                  <div class="container">
                     <div class="description__title">
                        Настенная сплит-система Aero ALRS-II-09IHA4-01/ALRS-II-09OHA4-01
                     </div>
                     <div class="description__text">
                        <p>
                           Это обновленная линейка кондиционеров предыдущего поколения, которые хорошо себя зарекомендовали
                           в 2016 году. Новые AeroLITE с улучшенными техническими и функциональными характеристиками,
                           отличаются простотой и удобством исполнения сплит системы, надежностью и доступной ценой.
                        </p>
                        <p>
                           Элегантный внутренний блок с классом энергоэффективности - «A» и оптимальный набор функций
                           создают идеальный баланс дизайна и современных технологий. Внутренний блок оснащен скрытым
                           дисплеем. Дисплей расположен на панели внутреннего блока, может быть включен или отключен при
                           помощи пульта ДУ.
                        </p>
                        <p>
                           Для очистки воздуха в корпусе внутреннего блока установлен фильтр холодного катализа, который
                           отлично очищает воздух от вредных примесей.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="change__characteristics characteristics">
               <div class="characteristics__desc">
                  <div class="container">
                     <div class="characteristics__desc--title">Характеристики</div>
                     <div class="characteristics__desc__body body">
                        <div class="type">
                           <span class="key">Тип кондиционера:</span>
                           <span class="dotted"></span>
                           <span class="value">Настенная сплит-система</span>
                        </div>
                        <div class="square">
                           <span class="key">Обслуживаемая площадь:</span>
                           <span class="dotted"></span>
                           <span class="value">18 кв.м.</span>
                        </div>
                        <div class="invertor">
                           <span class="key">Инвертор (плавная регулировка мощности):</span>
                           <span class="dotted"></span>
                           <span class="value">есть</span>
                        </div>
                        <div class="class">
                           <span class="key">Класс энергопотребления:</span>
                           <span class="dotted"></span>
                           <span class="value">А</span>
                        </div>
                        <div class="mode">
                           <span class="key">Основные режимы:</span>
                           <span class="dotted"></span>
                           <span class="value">охлаждение / обогрев</span>
                        </div>
                        <div class="range">
                           <span class="key">Диапазон поддерживаемых температур:</span>
                           <span class="dotted"></span>
                           <span class="value">16 - 32°С</span>
                        </div>
                        <div class="power-in-cold">
                           <span class="key">Мощность в режиме охлаждения:</span>
                           <span class="dotted"></span>
                           <span class="value">2650 Вт</span>
                        </div>
                        <div class="power-in-hot">
                           <span class="key">Мощность в режиме обогрева:</span>
                           <span class="dotted"></span>
                           <span class="value">2700 Вт</span>
                        </div>
                        <div class="power-input-in-cold">
                           <span class="key">Потребляемая мощность при обогреве:</span>
                           <span class="dotted"></span>
                           <span class="value">747 Вт</span>
                        </div>
                        <div class="power-input-in-hot">
                           <span class="key">Потребляемая мощность при охлаждении:</span>
                           <span class="dotted"></span>
                           <span class="value">825 Вт</span>
                        </div>
                        <div class="drainage">
                           <span class="key">Режим осушения:</span>
                           <span class="dotted"></span>
                           <span class="value">есть</span>
                        </div>
                        <div class="controller">
                           <span class="key">Пульт дистанционного управления:</span>
                           <span class="dotted"></span>
                           <span class="value">есть</span>
                        </div>
                        <div class="timer">
                           <span class="key">Таймер включения/выключения:</span>
                           <span class="dotted"></span>
                           <span class="value">есть</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="characteristics__features features">
                  <div class="container">
                     <div class="characteristics__features--title">Особенности</div>
                     <div class="features__title">
                        <h2>Режим тихой работы</h2>
                        <p>
                           Обеспечит снижение уровня шумя, тем самым создаёт оптимальные условия для Вашего отдыха.
                        </p>
                     </div>
                     <div class="features__title">
                        <h2>Режим сна</h2>
                        <p>
                           В этом режиме сплит система автоматически управляет температурой воздуха в помещении, делая Ваш
                           сон максимально комфортным.
                        </p>
                     </div>
                     <div class="features__title">
                        <h2>Функция автоматического перезапуска</h2>
                        <p>
                           В случае отключения электричества, сплит система автоматически перезапустится после
                           восстановления питания, с параметрами сохраненными в памяти.
                        </p>
                     </div>
                     <div class="features__title">
                        <h2>Функция самодиагностики</h2>
                        <p>
                           Позволяет отображать коды неисправностей на дисплее внутреннего блока, тем самым сокращая время
                           на поиски и устранение неисправности.
                        </p>
                     </div>
                     <div class="features__title">
                        <h2>Функция тёплый старт</h2>
                        <p>
                           Данная функция блокирует вентилятор внутреннего блока до тех пор, пока теплообменник не
                           прогреется, поэтому в Вашу комнату будут поступать потоки только теплого воздуха.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
<!-- ============================================== -->

<?php get_footer(); ?>