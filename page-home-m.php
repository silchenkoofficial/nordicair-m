<?php 
/*
Template Name: Главная m
*/
?>

<?php get_header(); ?>
<!-- === ГЛАВНАЯ НАВИГАЦИЯ =============================================== -->
        <section class="main-navigation">
   <div class="container">
      <div class="wrapper">
         <a href="conditioner-m" class="main-navigation__block block">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/content/main-navigation/conditioner.png" alt="Conditioner" class="block__icon">
            <h1 class="block__title">Кондиционирование</h1>
         </a>
         <a href="ventilation-m" class="main-navigation__block block">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/content/main-navigation/ventilation.png" alt="Conditioner" class="block__icon">
            <h1 class="block__title">Вентиляция</h1>
         </a>
         <a href="heating-m" class="main-navigation__block block">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/content/main-navigation/heating.png" alt="Conditioner" class="block__icon">
            <h1 class="block__title">Отопление</h1>
         </a>
      </div>
   </div>
</section>
<!-- ===================================================================== -->
<!-- === РЕКЛАМНЫЙ БЛОК ================================================== -->
        <section class="ad">
   <div class="container">
      <div class="wrapper"></div>
   </div>
</section>
<!-- ===================================================================== -->
<!-- === ЭТАПЫ РАБОТЫ ==================================================== -->
        <section class="stages">
   <div class="container">
      <div class="wrapper">
         <div class="stages__title">Этапы нашей работы</div>
         <div class="stages__main main">
            <div class="main__block block">
               <div class="block__img">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/content/stages/stages-1.png" alt="stage">
               </div>
               <div class="block__title">
                  <h1>Заказ товара</h1>
                  <p>
                     Выберите товар, положите его в корзину и оформите заказ
                  </p>
               </div>
            </div>
            <div class="main__arrow"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/content/stages/stages-arrow.png" alt=""></div>
            <div class="main__block block">
               <div class="block__img">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/content/stages/stages-2.png" alt="stage">
               </div>
               <div class="block__title">
                  <h1>Выезд на осмотр</h1>
                  <p>
                     Наш менеджер свяжется
                     с вами и договорится
                     о дате осмотра
                  </p>
               </div>
            </div>
            <div class="main__arrow"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/content/stages/stages-arrow.png" alt=""></div>
            <div class="main__block block">
               <div class="block__img">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/content/stages/stages-3.png" alt="stage">
               </div>
               <div class="block__title">
                  <h1>Доставка и установка</h1>
                  <p>
                     Как только ваш заказ будет готов, мы его доставим
                     и установим
                  </p>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- ===================================================================== -->
<!-- === ФОРМА =========================================================== -->
        <section class="form">
   <div class="container">
      <div class="wrapper">
         <form id="main-form" class="form__body">
            <label for="question" class="form__label">
               <p>Задайте вопрос</p>
               <input type="text" class="form__input" required>
            </label>
            <label for="tel" class="form__label">
               <p>Номер телефона</p>
               <input type="tel" class="form__input" required>
            </label>
            <label for="email" class="form__label">
               <p>Ваш Email</p>
               <input type="email" class="form__input" required>
            </label>
            <button class="form__button" form="main-form">Отправить</button>
         </form>
         <a href="#header" class="form__top-button">
            <i class="fa fa-arrow-circle-up" aria-hidden="true"></i>
            <p>В начало</p>
         </a>
      </div>
   </div>
</section>
<!-- ===================================================================== -->
<?php get_footer(); ?>