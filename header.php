<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>NordicAir -- климатическое оборудование</title>
        <meta name="theme-color" content="#fff">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=0, maximum-scale=1.0, minimum-scale=1.0">


        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/217a37f49f.css">

        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/main.css">
    </head>
    <body>
<!-- === HEADER ========================================================== -->
        <section class="header" id="header">
   <div class="container">
      <div class="wrapper">
         <a href="<?php bloginfo('url'); ?>" class="header__logo logo">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/header/header-logo.png" alt="Logo" class="logo__img">
            <div class="logo__text">
               <h1 class="logo__title">NordicAir</h1>
               <p class="logo__subtitle">
                  климатическое оборудование 
               </p>
            </div>
         </a>
         <nav class="header__nav nav">
            <div class="nav__buttons">
               <div class="nav__buttons--cart cart">
                  <i class="fa fa-shopping-cart" aria-hidden="true"></i>
               </div>
               <div class="nav__buttons--menu menu">
                  <span class="menu--lines line-1"></span>
                  <span class="menu--lines line-2"></span>
                  <span class="menu--lines line-3"></span>
               </div>
               <div class="nav__dropdown-menu dropdown">
                  <ul class="dropdown__list list">
                     <li><a href="javascript://">Настенные сплит-системы <img src="<?php echo get_template_directory_uri(); ?>/assets/images/footer/list/arrow.png" alt=""></a></li>
                     <li><a href="javascript://">Мульти сплит-системы <img src="<?php echo get_template_directory_uri(); ?>/assets/images/footer/list/arrow.png" alt=""></a></li>
                     <li><a href="javascript://">Полупромышленные сплит системы <img src="<?php echo get_template_directory_uri(); ?>/assets/images/footer/list/arrow.png" alt=""></a></li>
                     <li><a href="javascript://">Мульти сплит-системы <img src="<?php echo get_template_directory_uri(); ?>/assets/images/footer/list/arrow.png" alt=""></a></li>
                     <li><a href="javascript://">Вентиляция <img src="<?php echo get_template_directory_uri(); ?>/assets/images/footer/list/arrow.png" alt=""></a></li>
                     <li><a href="javascript://">Отопление <img src="<?php echo get_template_directory_uri(); ?>/assets/images/footer/list/arrow.png" alt=""></a></li>
                  </ul>
                  <div class="dropdown__close">
                     <span></span>
                     Закрыть меню
                  </div>
               </div>
            </div>
         </nav>
      </div>
   </div>
</section>
<!-- ===================================================================== -->
<!-- === ПОИСК =========================================================== -->
        <section class="search">
   <div class="container">
      <div class="wrapper">
         <div class="search__input">
            <input type="text" placeholder="Найти товар...">
            <i class="fa fa-search" aria-hidden="true"></i>
         </div>
      </div>
   </div>
</section>
<!-- ===================================================================== -->